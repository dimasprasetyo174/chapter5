import Button from "react-bootstrap/Button";
import React from "react";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { searchMovie } from "../api";
import { useNavigate } from "react-router-dom";


function NavbarItem() {
  const navigate = useNavigate()
  const search = async (q) => {
    const query = await searchMovie(q);
    console.log({ query: query });
  };

  return (
    <Navbar variant="dark">
      <Container>
        <Navbar.Brand href="#">
          <strong>Movielist</strong>
        </Navbar.Brand>
        <input placeholder="  what do you want to wacth ?" className="movie-search" onChange={({ target }) => search(target.value)} />
        <Nav>
          <Button className="me-3" variant="outline-danger" onClick={() => navigate("/login")}>
            Login
          </Button>
          <Button variant="danger" onClick={() => navigate("/register")}>Register</Button >
        </Nav>
      </Container>
    </Navbar>
  );
}

export default NavbarItem;
