import React from "react";
import { Button, Container, Navbar } from "react-bootstrap";
import { searchMovie } from "../api";
import { signOut } from "firebase/auth";
import { auth } from "../fireBaseConfige";

function NavbarItem2() {
  const search = async (q) => {
    const query = await searchMovie(q);
    console.log({ query: query });
  };
  async function logout() {
    await signOut(auth);
    localStorage.clear();
    window.location.reload();
  }

  return (
    <Navbar variant="dark">
      <Container>
        <Navbar.Brand>
          <strong>Movielist</strong>
        </Navbar.Brand>
        <input
          placeholder="  what do you want to wacth ?"
          className="movie-search"
          onChange={({ target }) => search(target.value)}
        />
        <Button
          className="ps-4 pe-4 p-1"
          variant="danger"
          onClick={logout}
          type="submit"
        >
          Logout
        </Button>
      </Container>
    </Navbar>
  );
}

export default NavbarItem2;
