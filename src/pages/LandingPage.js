import React from "react";
import Cover from "../components/Cover";
import NavbarItem from "../components/NavbarItem";
import Popular from "../pages/Popular";

function LandingPage() {
  return (
    <div className="Home">
      <NavbarItem />
      <Cover />
      <Popular />
    </div>
  );
}

export default LandingPage;
