import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import { Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { auth } from "../fireBaseConfige";

function Register() {
  const navigate = useNavigate();
  const [registationEmail, setRegistationEmail] = useState("");
  const [registationPassword, setRegistationPassword] = useState("");
  const [registationError, setRegistationError] = useState(null);

  async function register() {
    setRegistationError(null);
    try {
      await createUserWithEmailAndPassword(
        auth,
        registationEmail,
        registationPassword
      );
    } catch (e) {
      setRegistationError(e);
    }
  }

  function myFunction() {
    var x = document.getElementById("myInput");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }

  return (
    <div className="Register">
      <h2>
        <strong>Sign up</strong>
      </h2>
      <Form className="registerForm">
        <Form.Group className="mb-3 " controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={registationEmail}
            onChange={(ev) => setRegistationEmail(ev.target.value)}
          />
        </Form.Group>

        <Form.Group className="mb-3 " controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            id="myInput"
            className="mb-2"
            type="password"
            placeholder="Password"
            value={registationPassword}
            onChange={(ev) => setRegistationPassword(ev.target.value)}
          />
          <div className="radioButton">
            <input
              className="me-2"
              type="checkbox"
              onClick={() => myFunction()}
            />
            Show Password
          </div>
          <p>{registationError?.message}</p>
        </Form.Group>
        <div className="registerButton">
          <Button
            className="ps-4 pe-4 p-1"
            variant="danger"
            onClick={register}
            type="submit"
          >
            Register
          </Button>
          <Button
            className="ps-4 pe-4 p-1 mt-3"
            variant="outline-danger"
            onClick={() => navigate("/login")}
          >
            To Login
          </Button>
        </div>
      </Form>
    </div>
  );
}

export default Register;
