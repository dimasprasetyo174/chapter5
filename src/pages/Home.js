import React from "react";
import Cover from "../components/Cover";
import Popular from "./Popular";
import NavbarItem2 from "../components/NavbarItem2";

function Home() {
  return (
    <div className="Home2">
      <NavbarItem2 />
      <Cover />
      <Popular />
    </div>
  );
}

export default Home;
