import React, { useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import { useNavigate } from "react-router-dom";
import { getMovieList } from "../api";

function Popular() {
  const navigate = useNavigate();
  const [popularMovie, setPopularMovie] = useState([]);

  useEffect(() => {
    getMovieList().then((result) => {
      setPopularMovie(result);
    });
  }, []);

  const PopularMovieList = () => {
    return popularMovie.map((movie, i) => {
      return (
        <div className="movie-wrapper" key={i}>
          <div className="movie-title">{movie.title}</div>
          <img className="movie-image" src={`${process.env.REACT_APP_BASEIMGURL}/${movie.poster_path}`} />
          <div className="movie-rate">{movie.vote_average}</div>
        </div>
      );
    });
  };

  return (
    <div className="Popular mt-4 ms-2 me-2">
      <div className="popular-top ms-3">
        <h2>Popular Movie</h2>
        <Button className="me-3" variant="outline-danger" onClick={() => navigate("/pencarian")}>
          Search More Movie
        </Button>
      </div>
      <div className="movie-container">
        <PopularMovieList />
      </div>
    </div>
  );
}

export default Popular;
