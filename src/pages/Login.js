import React, { useEffect, useState } from "react";
import Home from "./Home";
import { Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import {
  onAuthStateChanged,
  signInWithEmailAndPassword,
  signInWithPopup,
} from "firebase/auth";
import { auth, provider } from "../fireBaseConfige";

function Login() {
  const [user, setUser] = useState(null);
  const [loginEmail, setLoginEmail] = useState("");
  const [loginPassword, setLoginPassword] = useState("");
  const [loginError, setLoginError] = useState(null);
  const navigate = useNavigate();

  async function login() {
    try {
      setLoginError(null);
      await signInWithEmailAndPassword(auth, loginEmail, loginPassword);
    } catch (e) {
      setLoginError(e);
    }
  }

  function myFunction() {
    var x = document.getElementById("myInput");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }
  const HandleClik = () => {
    signInWithPopup(auth, provider).then((data) => {
      setUser(data.user.email);
      localStorage.setItem("email", data.user.email);
    });
  };
  useEffect(() => {
    setUser(localStorage.getItem("email"));
  });

  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      setUser(user);
    });
  }, []);

  return (
    <div>
      {!!user && (
        <div>
          <Home />
        </div>
      )}
      {!user && (
        <div className="Login">
          <h2>
            <strong>Login</strong>
          </h2>
          <form className="loginForm">
            <label>Email</label>
            <input
              className="inputLoginEmail"
              placeholder="  Email"
              type="text"
              value={loginEmail}
              onChange={(ev) => setLoginEmail(ev.target.value)}
            />
            <label>Password</label>
            <input
              id="myInput"
              className="inputLoginPassword"
              placeholder="  Password"
              type="password"
              value={loginPassword}
              onChange={(ev) => setLoginPassword(ev.target.value)}
            />
            <div className="radioButton">
              <input
                className="me-2"
                type="checkbox"
                onClick={() => myFunction()}
              />
              Show Password
            </div>
          </form>
          <p>{loginError?.message}</p>
          <div className="loginButton">
            <Button
              className="ps-5 pe-5 buttonLogin"
              variant="danger"
              onClick={login}
            >
              <strong>Login</strong>
            </Button>
            <Button
              className="ps-5 pe-5"
              variant="outline-danger"
              onClick={() => navigate("/")}
            >
              <strong>Cancel</strong>
            </Button>
          </div>
          <div className="signInGoogle">
            <button onClick={HandleClik}>Sign in With Google</button>
          </div>
        </div>
      )}
    </div>
  );
}

export default Login;
